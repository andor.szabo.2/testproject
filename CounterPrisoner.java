package Other;

public class CounterPrisoner extends Prisoner{

	private long counter = 0L;
	
	public CounterPrisoner(long id) {
		super(id);
	}

	public long getCounter() {
		return counter;
	}

	public void setCounter(long counter) {
		this.counter = counter;
	}
	
	

}
