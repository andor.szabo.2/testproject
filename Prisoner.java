package Other;

public class Prisoner {
	
	private long id;
	private boolean hasBeenOnWalk = false;
	private boolean switched = false;
	private int down = 0; 
	
	
	public Prisoner(long id) {
		super();
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public boolean isHasBeenOnWalk() {
		return hasBeenOnWalk;
	}
	
	public void setHasBeenOnWalk(boolean hasBeenonWalk) {
		this.hasBeenOnWalk = hasBeenonWalk;
	}
	public boolean isSwitched() {
		return switched;
	}
	
	public void setSwitched(boolean switched) {
		this.switched = switched;
	}
	
	public void turnSwitch(Switch s){
		s.set(true);		
	}
	

	public int getDown() {
		return down;
	}

	public void setDown(int down) {
		this.down = down;
	}

	@Override
	public String toString() {
		return "Prisoner [id=" + id + ", hasBeenOnWalk=" + hasBeenOnWalk + ", switched=" + switched + ", down=" + down
				+ "]";
	}

	
	
}
