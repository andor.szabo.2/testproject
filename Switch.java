package Other;

public class Switch {
	
	private boolean turnedOn = false;

	public boolean isTurnedOn() {
		return turnedOn;
	}

	public void set(boolean b) {
		this.turnedOn = b;
	}

}
