package Other;

import java.util.HashMap;
import java.util.Random;

public class Simulation {

	private static int numberOfSwitchDown = 0;
	private static int days = 0;
	private static final int NUMBER_OF_PRISONERS = 100;
	private static Switch sw = new Switch();
	private static Prisoner randomPrisoner;
	private static HashMap<Integer, Prisoner> prisoners = new HashMap<Integer, Prisoner>();
	private static Random rand = new Random();

	public static void main(String[] args) {

		for (int i = 0; i < NUMBER_OF_PRISONERS; i++) {
			prisoners.put(new Integer(i), new Prisoner(i));
		}
		prisoners.put(new Integer(NUMBER_OF_PRISONERS), new CounterPrisoner(NUMBER_OF_PRISONERS));

		while (numberOfSwitchDown != NUMBER_OF_PRISONERS - 1) {
			randomPrisoner = prisoners.get(rand.nextInt(NUMBER_OF_PRISONERS + 1));
			//System.out.println(randomPrisoner.toString() + " " + "DAY:" + days);
			if (randomPrisoner.getId() == NUMBER_OF_PRISONERS) {
				if (sw.isTurnedOn()) {
					sw.set(false);
					numberOfSwitchDown++;
					System.out.println("SW turend down " + numberOfSwitchDown + " times, days passed: " + days);
				}
			} else {
				if (!sw.isTurnedOn() && !randomPrisoner.isSwitched()) {
					randomPrisoner.turnSwitch(sw);
					randomPrisoner.setSwitched(true);
				}
			}
			days++;
		}
	}
}
